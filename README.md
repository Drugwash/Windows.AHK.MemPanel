![](ss_mempanel1106.png)

Monitors RAM, swap, CPU usage.
Display organization as follows:

     Free RAM   [______] Total RAM
     Free swap  [______] Total swap
    Memory load [______] CPU load

• Uses `GlobalMemoryStatus/Ex()` API.     
• Not tested (much) on x64 systems.     
• Drag with the mouse to position.      
• Right-click to hide to tray.		
• Double-click its tray icon to show.		
• Use tray menu **> Options** to change skin and/or polling interval.

![](ss_mempanel1106_options.png)

**© Drugwash, 2012-2023**
